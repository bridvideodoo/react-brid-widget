This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

`<ReactBridWidget>` is a React Component for initializing client-side instances of a Brid playlist widget. Simply give `<ReactBridWidget>` a couple of configuration parameters and you are good to go.

## Contents

- [Installation](#markdown-header-installation)
- [Usage](#markdown-header-usage)
- Props
  - [Required Props](#markdown-header-required-props)
  - [Other Props](#markdown-header-other-props)
- [Examples](#markdown-header-examples)
- [Contributing](#markdown-header-contributing)

## Installation

```shell
npm install react-brid-widget
```

## Usage

At the mininum, you can just use something like the below code snippet:

### Displaying a Brid Playlist Widget

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import ReactWidgetPlayer from 'react-brid-widget';

ReactDOM.render(<ReactWidgetPlayer
  divId='Brid_12345'
  id='8875'
  partner='7612'
  mobileHeight='400'
  height='400'
  playlistId='6989'
  color='ffffff'
  playlistType='0'
  autoplay='0'
  shuffle='0'
/>, document.getElementById("root"));
```

## Required Props

These are props that modify the basic behavior of the component.

- **`divId`**
    - The ID of the DIV in which the playlist widget will render in.
    - Type: `string`
    - Example: `divId="Brid_12345"`
- **`id`**
    - The ID of the Brid player you want to use. To get this ID for your account please see this entry [here](https://developer.brid.tv/brid-player/javascript-api-reference)
    - Type: `string`
    - Example: `id="12345"`
- **`partner`**
    - The ID of your web property in Brid. To get this ID for your account please see this entry [here](https://developer.brid.tv/brid-player/javascript-api-reference)
    - Type: `string`
    - Example: `id="12345"`
- **`mobileHeight`**
    - Set the mobile height in pixels for your playlist widget. Also accepted values here are percentages like 100%.
    - Type: `string`
    - Example: `mobileHeight="400"`
- **`height`**
    - Set the height in pixels of your playlist widget. Also accepted values here are percentages like 100%.
    - Type: `string`
    - Example: `height="100%"`
- **`playlistId`**
    - If you want to embed a custom made playlist inside your Brid playlist widget, pass a playlist ID as a prop.
    - Type: `string`
    - Example: `playlist="12345"`
- **`autoplay`**
    - Set the widget to autoplay or not. Accepted values are 0 or 1.
    - Type: `string`
    - Example: `autoplay="0"`
- **`shuffle`**
    - Set the widget to randomly shuffle videos inside your playlist. Accepted values are 0 or 1.
    - Type: `string`
    - Example: `shuffle="1"`

## Other Props

These are props that you can use to embed dynamic playlists with the Brid playlist widget. In this case ommit the "playlistId" prop for your component initialization.

- **`playlistType`**
    - This is a number that represents a certain type of playlist in Brid. Latest video playlists always have a value of 0. Other possible values are 2 and 5. 
    - Type: `string`
    - Example: `playlistType="0"` Should be used to get a feed of your latest videos under your account.
    - Example: `playlistType="2"` Should be used to get a feed of your latest videos under your account that belong to a specific channel/category.
    - Example: `playlistType="5"` Should be used to get a feed of your latest videos under your account that have a specific tag.
- **`categoryId`**
    - Use this prop to pass a dynamic playlist by category/channel. Populate this prop with the appropriate category/channel ID. 
    - Type: `string`
    - Example: `categoryId="1234"`
- **`tag`**
    - Use this prop to pass a dynamic playlist by tag. Populate this prop with an appropriate tag for example tag='sports'. 
    - Type: `string`
    - Example: `tag="1234"`

## Examples

See below a couple of examples on how to set up your playlist widget react component to load dynamic playlists.

#### Set up a playlist widget with your accounts latest videos playlist

```
ReactDOM.render(<ReactWidgetPlayer
  divId='Brid_12345'
  id='1234'
  partner='1234'
  mobileHeight='400'
  height='400'
  color='ffffff'
  playlistType='0'
  autoplay='0'
  shuffle='0'
/>, document.getElementById("root"));
```

#### Set up a playlist widget with your accounts latest videos playlist by channel/category

```
ReactDOM.render(<ReactWidgetPlayer
  divId='Brid_12345'
  id='1234'
  partner='1234'
  mobileHeight='400'
  height='400'
  color='ffffff'
  playlistType='2'
  categoryId='13'
  autoplay='0'
  shuffle='0'
/>, document.getElementById("root"));
```

#### Set up a playlist widget with your accounts latest videos playlist by specific tag

```
ReactDOM.render(<ReactWidgetPlayer
  divId='Brid_12345'
  id='1234'
  partner='1234'
  mobileHeight='400'
  height='400'
  color='ffffff'
  playlistType='5'
  tag='sports'
  autoplay='0'
  shuffle='0'
/>, document.getElementById("root"));
```

## Contributing

First, thank you for taking the time to contribute something to this Brid react component!
Your help is always welcome! Feel free to open issues, ask questions, talk about it and discuss.

### Pull Requests

When contributing to this repo, please first discuss the change you wish to make via issue, email, or any other method.

### Feature Requests

I am always interested in expanding the feature-set of this component so if there is something you need, just open a ticket [here](https://brid.zendesk.com/hc/en-us/requests/new) or [drop me a line](mailto:support@brid.tv).

