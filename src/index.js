import React from "react";
import ReactDOM from "react-dom";
import ReactWidgetPlayer from "./ReactWidgetPlayer";

// For custom playlists add playlistId='<YOUR PLAYLIST ID>'
// For latest video feeds make sure playlistType is set to 0
// For per channel feeds set playlistType to 2 and add categoryId='<BRID CHANNEL ID>'
// For pre tag feeds set playlistType to 5 and add tag='<YOUR CUSTOM TAG>'

ReactDOM.render(<ReactWidgetPlayer
  divId='Brid_12345'
  id='8875'
  partner='7612'
  mobileHeight='400'
  height='400'
  playlistId='6989'
  color='ffffff'
  playlistType='0'
  autoplay='0'
  shuffle='0'
/>, document.getElementById("root"));

if (module.hot) {
  module.hot.accept();
}
