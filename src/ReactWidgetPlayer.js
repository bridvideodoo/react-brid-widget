import React, { Component } from "react";

import injectWidgetScript from "./helpers/injectScripts";
import removeBridWidget from "./helpers/removeWidget";

const displayName = "ReactWidgetPlayer";
class ReactWidgetPlayer extends Component {
  constructor(props) {
    super(props);
    this.scriptSrc = "//services.brid.tv/widget/brid.widget.min.js";
    this.uniqueScriptId = "brid-widget-script";
    this.widgetConfig = {
      id: this.props.divId,
      playerId: this.props.id,
      height: this.props.height,
      mobileHeight: this.props.mobileHeight,
      playlistId: this.props.playlistId,
      color: this.props.color,
      playlistType: this.props.playlistType,
      autoplay: 0,
      shuffle: 0
      //stats: {"rj" : 1}
    };
    //(this.props.video) ? this.playerConfig.video = this.props.video : this.playerConfig.playlist = this.props.playlist;
    this.state = { ready: "false", div: {} };
  }

  componentDidMount() {
    injectWidgetScript({
      context: document,
      onLoadCallback: this.initialize,
      scriptSrc: this.scriptSrc,
      uniqueScriptId: this.uniqueScriptId
    });
  }

  componentWillUnmount() {
    removeBridWidget(this.props.divId, window);
  }

  initialize = () => {
    //const player = window.$bp(this.props.divId, this.widgetConfig);
    //this.setState({ ready: true, div: player });
    console.log(this.widgetConfig);
    window.$bWidgets.init(this.widgetConfig);
    //this.eventHandlers = createEventHandlers(this, window.$bp);
  }

  render() {
    return (
      <div
        id={this.props.divId}
        //className="brid"
        //style={{ width: this.props.width, height: this.props.height }}
      />
    );
  }
}

ReactWidgetPlayer.displayName = displayName;
export default ReactWidgetPlayer;
