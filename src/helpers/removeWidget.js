function removeBridWidget(playerId, context) {
    const player = context.$bp
    if (player) {
      player(playerId).destroy();
    }
  }
  
  export default removeBridWidget;