function injectWidgetScript({
    context,
    onLoadCallback,
    scriptSrc,
    uniqueScriptId
  }) {
    const bridWidgetScript = context.createElement("script");
    bridWidgetScript.id = uniqueScriptId;
    bridWidgetScript.src = scriptSrc;
    bridWidgetScript.onload = onLoadCallback;
  
    context.head.appendChild(bridWidgetScript);
  }
  
  export default injectWidgetScript;
  